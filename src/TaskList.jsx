import React,{useState} from 'react'
import Task from './Task'

const TaskList = ({tasks,onChangeTask,onDeleteTask}) => {
    
     return (
          <div>
               {tasks.map(t=>(
                   <Task key={t.id} t={t} onChangeTask={onChangeTask} onDeleteTask={onDeleteTask}/>
               ))}
               
          </div>
     )
}

export default TaskList