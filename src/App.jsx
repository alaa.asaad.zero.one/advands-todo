import { useReducer } from 'react';
import AddTask from './AddTask';
import TaskList from './TaskList';
import tasksReducer from './taskReducer';

export default function App() {
  const [tasks, dispatch] = useReducer(tasksReducer, initialTasks);

  function handleAddTask(text) {

    dispatch({
      type: 'added',
      id: nextId++,
      text: text,
    });
  }

  function handleChangeTask(task) {
    dispatch({
      type: 'changed',
      task: task,
    });
  }

  function handleDeleteTask(taskId) {
    dispatch({
      type: 'deleted',
      id: taskId,
    });
  }

  return (
    <>
      <h1>Todo List</h1>
      <AddTask onAddTask={handleAddTask} />
      <TaskList
        tasks={tasks}
        onChangeTask={handleChangeTask}
        onDeleteTask={handleDeleteTask}
      />
    </>
  );
}

let nextId = 3;
const initialTasks = [
  { id: 0, title: 'todo 1', text: 'Visit React', done: true, createdAt: '2023/3/1', finishedAt: '2023/3/19', archiveAt: '' },
  { id: 1, title: 'todo 2', text: 'coding python', done: false, createdAt: '2023/3/1', finishedAt: '2023/3/19', archiveAt: '' },
  { id: 2, title: 'todo 3', text: 'Lennon Docker', done: false, createdAt: '2023/3/1', finishedAt: '2023/3/19', archiveAt: '' },
];
