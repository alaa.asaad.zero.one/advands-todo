import React,{useState} from 'react'

const AddTask = ({onAddTask}) => {
     const [text,setText]= useState('')
     return (
          <div>
               <input value={text} onChange={(e)=>setText(e.target.value)}/>
               <button onClick={()=>onAddTask(text)}>Add Todo</button>
          </div>
     )
}

export default AddTask