import React, { useEffect, useState } from 'react'

const Task = ({ t, onDeleteTask, onChangeTask }) => {
     const [edit, setEdit] = useState(false)
     const [done, setDone] = useState(false)
     const [showInfo, SetShowInfo] = useState(false)


     return (
          <div>
               <input type='checkbox' value={t.done} checked={t.done} onChange={(e) =>
                    onChangeTask({
                         ...t,
                         done: !t.done
                    })} />
               {edit ?
                    <input placeholder={t.text} defaultValue={t.text}
                         onChange={(e) => {
                              onChangeTask({
                                   ...t,
                                   text: e.target.value === '' ? t.text : e.target.value
                              })
                         }} />
                    :
                    <p>{t.text}</p>}
               <button onClick={() => onDeleteTask(t.id)}>Delete</button>
               <button onClick={() => { setEdit(prv => !prv) }}>
                    {edit ? "Save" : "Edit"}</button>
               <button onClick={() => SetShowInfo(prv => !prv)}>Show Info</button>
               <button onClick={() => console.log(new Date().toString())}>show date</button>
               {showInfo ? (
                    <>
                         <p>{t.id}</p>
                         <p>{t.title}</p>
                         <p>{t.text}</p>
                         <p>{t.createdAt}</p>
                         <p>{t.finishedAt}</p>
                         <p>{t.archiveAt === '' ? 'Not Archived Yet' : t.archiveAt}</p>

                    </>
               ) : null}
          </div>
     )
}

export default Task